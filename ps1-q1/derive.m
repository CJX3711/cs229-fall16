function [ sum ] = derive( x, y, theta, i )
%DERIVE Summary of this function goes here
%   Detailed explanation goes here
    
    sum = 0;
    m = size(y,1);
    
    for k = 1:m
      xk = [1, x(k,:)]';
      yk = y(k);
      z = yk * theta' * xk;
      h = g(z);
      sum = sum + ( 1 - h ) * xk(i) * yk;
    end
    
    sum = sum / m;
    sum = - sum;
end

