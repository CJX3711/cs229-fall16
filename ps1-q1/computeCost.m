function J = computeCost(X, y, theta)
%COMPUTECOST Summary of this function goes here
%   Detailed explanation goes here
  sum = 0;
  for i = 1:length(X)
      X_i = transpose(X(i,:)); % Make a column vector
      y_i = y(i); % Scalar
      sum = sum + (y_i - transpose(theta) * X_i) ^ 2;
  end
  sum = sum * 0.5;
  J = sum;
end
