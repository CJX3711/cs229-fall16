function [ gz ] = g( z )
  gz = 1 / (1 + exp(-z));
end
