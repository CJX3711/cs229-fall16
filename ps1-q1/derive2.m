function [ sum ] = derive2( x, y, theta, i, j )
%DERIVE2 Summary of this function goes here
%   Detailed explanation goes here

  sum = 0;
  m = size(y,1);

  for k = 1:m
    xk = [1, x(k,:)]';
    yk = y(k);
    z = yk * theta' * xk;
    h = g(z);
    sum = sum + h * ( 1 - h ) * xk(i) * xk(j) * yk^2 ;
  end

  sum = sum / m;
end

