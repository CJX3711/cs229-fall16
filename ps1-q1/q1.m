% CS229 Autumn 2016
% Problem Set 1 - Supervised Learning
% Question 1(b)
X = load('logistic_x.txt'); % x is a m x n matrix
y = load('logistic_y.txt'); % y is a m x 1 matrix

n = size(X,2); % number of features
m = length(y); % number of training examples
theta = zeros( n+1, 1 ); % Create a n+1 x 1 matrix (col vector)

itr = 0;
old_value = 0;
while 1
  itr = itr + 1;
  thetad = zeros( n+1, 1);
  for it = 1:n+1
    thetad(it) = derive(X, y, theta, it);
  end

  hessian = zeros(n+1, n+1);
  
  for hi = 1:n+1
    for hj = 1:n+1
      hessian(hi,hj) = derive2(X, y, theta, hi, hj);
    end
  end

  new_theta = theta - inv(hessian) * thetad;
  theta = new_theta;
  
  value = abs(theta(1)) + abs(theta(2)) + abs(theta(3));
  delta = abs(value - old_value);
  disp(delta);
  if delta < 0.00001
      break
  end
  old_value = value;
end


positive = find(y == 1);
negative = find(y == -1);
positiveX = X(positive,:);
negativeX = X(negative,:);


x1_min = min(X(:,1))-1;
x1_max = max(X(:,1))+1;
x2_min = (-theta(1) - theta(2) * x1_min)/theta(3);
x2_max = (-theta(1) - theta(2) * x1_max)/theta(3);

hold on; % keep previous plot visible
scatter(positiveX(:,1), positiveX(:,2), 10, 'filled', 'o');
scatter(negativeX(:,1), negativeX(:,2), 10, 'o');
plot([x1_min x1_max], [x2_min x2_max]);
legend('Positive', 'Negative', 'Decision Boundary');
