function [ s ] = d_right( f1, f2 )
%D f1 and f2 should be of size 1 x m
%   Calculates the distance of the f_right data
  s = 0;
  for i = 1300:1599
    ind = freq2ind(i);
    s = s + (f1(ind) - f2(ind))^2;
  end
end

