load_quasar_data

totalError = check_error(train_qso, lambdas, 5, 3);
disp('Training: ')
disp(totalError);

[ totalError, smoothed, f_left_hat ] = check_error(test_qso, lambdas, 5, 3);
disp('Test: ')
disp(totalError);

figure
i = 1;
hold on;
from = 1;
to = 70;
plot(lambdas(:),smoothed(i,:));
plot(lambdas(from:to),f_left_hat(i,from:to));
legend('Smooth', 'f_l_e_f_t^h^a^t');
title('Test example 1')

figure;
hold on;
i = 6;
plot(lambdas(:),smoothed(i,:));
plot(lambdas(from:to),f_left_hat(i,from:to));
legend('Smooth', 'f_l_e_f_t^h^a^t');
title('Test example 6')
