function[ output ] = func_h( train_qso, ind )
%H train_qso is the whole array, f is a single entry of train_qso
%   Calculates the distance of the f_right data
    f = train_qso(ind,:);
    
    m = size(train_qso, 1);
    max_diff = 0;
    for i = 1:m
        if i == ind
            continue
        end
        diff = d_right( train_qso(i,:), f );
        if diff > max_diff
            max_diff = diff;
        end
    end
    output = max_diff;

end