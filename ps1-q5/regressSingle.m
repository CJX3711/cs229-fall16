function [ fx ] = regressSingle( X, y, tau )
%REGRESSSINGLE Summary of this function goes here
%   Detailed explanation goes here
    x = X(:,2);
    n = size(y, 1);
    W = eye(n);
    fx = zeros(n,1);
    for i = 1:n
        curx = x(i);
        for j = 1:n
            xj = x(j);
            w = exp( -((curx-xj)^2)/(2*tau^2) );
            W(j,j) = w/2;
        end
        theta = ((transpose(X)*W*X)^(-1))*transpose(X)*W*y;
        fx(i) = curx * theta(2) + theta(1);
    end
end

