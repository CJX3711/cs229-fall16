load_quasar_data

[m , n] = size(train_qso);
X = [ones(n,1), lambdas];
x = lambdas;
y = transpose(train_qso(1,:));

theta = ((transpose(X)*X)^(-1))*transpose(X)*y;

disp('Theta');
disp(theta);
figure;
hold on;
scatter(x,y, 2, 'x');
plot(x ,theta(2) * x + theta(1));
legend('Scatter plot', 'Linear');
title('Q5b i. Linear plot');

figure
hold on;
scatter(x,y, 2, 'x');
fx = regressSingle( X, y, 5 );
plot(x,fx);
legend('Scatter plot', 'Weighted \tau = 5');
title('Q5b ii. Weighted Linear Regression plot');

figure
hold on;
scatter(x,y, 2, 'x');
title('Q5b iii. Weighted Linear Regression plot with varying tau');
fx = regressSingle( X, y, 1 );
plot(x,fx);

fx = regressSingle( X, y, 10 );
plot(x,fx);

fx = regressSingle( X, y, 100 );
plot(x,fx);

fx = regressSingle( X, y, 1000 );
plot(x,fx);

legend('Scatter plot', 'Weighted tau = 1','Weighted tau = 10', 'Weighted tau = 100', 'Weighted tau = 1000');
