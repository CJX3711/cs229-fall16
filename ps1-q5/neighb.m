function [ closest ] = neighb( train_qso, ind, k )
%NEIGHB Summary of this function goes here
%   Detailed explanation goes here
    f = train_qso(ind,:);
    m = size(train_qso, 1);
    distanceMap = zeros(m,2);
    for i = 1:m
      distanceMap(i,2) = i;
      if i == ind
        continue
      end
      distanceMap(i,1) = d_right(train_qso(i,:),f);
    end
    distanceMap = sortrows(distanceMap);
    distanceMap = distanceMap(:,2);
    closest = distanceMap(1:k);
end
