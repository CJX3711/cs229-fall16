function [ totalError, smooth_qso, left_qso ] = check_error( qso_data, lambdas, maxI, k )
%CHECK_ERROR Checks the error of the training set
    % Smooth the data
    [m , n] = size(qso_data);
    X = [ones(n,1), lambdas];
    x = lambdas;
    smooth_qso = zeros(size(qso_data));
    left_qso = zeros(size(qso_data));
    for itr = 1:maxI
      y = transpose(qso_data(itr,:));
      fxi = regressSingle( X, y, 5 );
      smooth_qso(itr,:) = fxi;
    end

    % Find the error
    totalError = 0;
    for j = 1:maxI
        neigh_ind = neighb(smooth_qso, j, k);
        neigh_f = smooth_qso(neigh_ind,:);
        f_j = smooth_qso(j,:);

        top = zeros(size(f_j));
        bottom = 0;
        for itr = 1:k
          f_i = neigh_f(itr,:);
          dist = d_right(f_i, f_j);
          h = func_h(smooth_qso, j);
          kerThing = ker(dist/h);
          top = top + kerThing * f_i;
          bottom = bottom + kerThing;
        end

        f_left_hat = top / bottom;
        left_qso(j,:) = f_left_hat;
        dleft = d_left(f_left_hat, f_j);
        totalError = totalError + dleft ;
    end
    totalError = totalError / maxI;

end
