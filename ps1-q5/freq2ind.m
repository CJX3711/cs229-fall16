function [ output ] = freq2ind( lambda )
%F2ind
%   Maps the wavelength to the array index
    output = lambda - 1149;
end

