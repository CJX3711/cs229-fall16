function [ind, thresh] = find_best_threshold(X, y, p_dist)
% FIND_BEST_THRESHOLD Finds the best threshold for the given data
%
% [ind, thresh] = find_best_threshold(X, y, p_dist) returns a threshold
%   thresh and index ind that gives the best thresholded classifier for the
%   weights p_dist on the training data. That is, the returned index ind
%   and threshold thresh minimize
%
%    sum_{i = 1}^m p(i) * 1{sign(X(i, ind) - thresh) ~= y(i)}
%
%   OR
%
%    sum_{i = 1}^m p(i) * 1{sign(thresh - X(i, ind)) ~= y(i)}.
%
%   We must check both signed directions, as it is possible that the best
%   decision stump (coordinate threshold classifier) is of the form
%   sign(threshold - x_j) rather than sign(x_j - threshold).
%
%   The data matrix X is of size m-by-n, where m is the training set size
%   and n is the dimension.
%
%   The solution version uses efficient sorting and data structures to perform
%   this calculation in time O(n m log(m)), where the size of the data matrix
%   X is m-by-n.

[mm, nn] = size(X);
ind = 1;
thresh = 0;
err = -1;
% ------- Your code here -------- %

% sum_{i = 1}^m p(i) * 1{sign(X(i, ind) - thresh) ~= y(i)}
for j = 1 : nn % loop for each feature
    fyp_matrix_j = [X(:,j),y,p_dist];
    fyp_matrix_j = sortrows(fyp_matrix_j, -1); %sort rows in descending Xj
    curr_thresh_j = fyp_matrix_j(1,1)+1; %set a threshold that is larger than the largest Xj(i)
    y_j = fyp_matrix_j(:,2);
    p_j = fyp_matrix_j(:,3);
    y_j(y_j==-1)=0; % Set the correctly classified examples to 0 to be ignored.
    curr_err_j = p_j'*y_j;

    max_err_j = 0;
    max_thresh_j = curr_thresh_j;
    err_j = curr_err_j;
    thresh_j = curr_thresh_j;

    for i = 2 : mm + 1 %loop for each set
      if ( i == mm + 1 )
        curr_thresh_j = fyp_matrix_j(mm,1)-1; %last case
      else
        curr_thresh_j = (fyp_matrix_j((i-1), 1) + fyp_matrix_j(i, 1)) / 2;
      end
        x_poi = fyp_matrix_j((i-1), 1);
        y_poi = fyp_matrix_j((i-1), 2);
        p_poi = fyp_matrix_j((i-1), 3);

        if (y_poi == 1) %WE JUST MADE IT RIGHT
            curr_err_j = curr_err_j - p_poi;
        else %WE JUST MADE IT WRONG
            curr_err_j = curr_err_j + p_poi;
        end

        if (curr_err_j < err_j)
            err_j = curr_err_j;
            thresh_j = curr_thresh_j;
        end
        if (curr_err_j > max_err_j)
            max_err_j = curr_err_j;
            max_thresh_j = curr_thresh_j;
        end
    end

    %check to see if the negative classifier gives a lower error.
    if ((1-max_err_j) < err_j)
       err_j = 1 - max_err_j;
       thresh_j = max_thresh_j;
    end

    %make the check against the main min err
    if (err == -1)
        err = err_j;
        % ind = 1;
        thresh = thresh_j;
    elseif (err_j < err)
        err = err_j;
        ind = j;
        thresh = thresh_j;
    else
    end

end

%
% A few hints: you should loop over each of the nn features in the X
% matrix. It may be useful (for efficiency reasons, though this is not
% necessary) to sort each coordinate of X as you iterate through the
% features.
