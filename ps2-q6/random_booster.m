function [theta, feature_inds, thresholds] = random_booster(X, y, T)
    % RANDOM_BOOSTER Uses random thresholds and indices to train a classifier
    %
    % [theta, feature_inds, thresholds] = random_booster(X, y, T)
    %  performs T rounds of boosted decision stumps to classify the data X,
    %  which is an m-by-n matrix of m training examples in dimension n.
    %
    %  The returned parameters are theta, the parameter vector in T dimensions,
    %  the feature_inds, which are indices of the features (a T dimensional vector
    %  taking values in {1, 2, ..., n}), and thresholds, which are real-valued
    %  thresholds. The resulting classifier may be computed on an n-dimensional
    %
    %   theta' * sgn(x(feature_inds) - thresholds).

    [mm, nn] = size(X);
    p_dist = ones(mm, 1);
    p_dist = p_dist / sum(p_dist);

    theta = [];
    feature_inds = [];
    thresholds = [];

    for iter = 1:T
        new_p_dist = [];
        % Step 1: Update the weights
        for i = 1:mm
            sum_i = 0;
            for j = 1:iter-1
                if X(i, feature_inds(j)) >= thresholds(j)
                    predicted_x_i = 1;
                else
                    predicted_x_i = -1;
                end
                sum_i = sum_i + theta(j)*predicted_x_i;
            end
            w_i = exp( -y(i) * sum_i );
            new_p_dist = [ new_p_dist; w_i];
        end
        sum_p = sum(new_p_dist);
        p_dist = new_p_dist/sum_p;

        ind = ceil(rand * nn);
        thresh = X(ceil(rand * mm), ind) + 1e-8 * randn;

        % fprintf(1, 'Random Ind: %d Thresh: %1.4f\n', ind, thresh);


        % ---- Your code here ----- %
        % Step 3: Compute theta
        sum_w_plus = 0;
        sum_w_minus = 0;
        for i = 1:mm
            % i is the row representing the ith training set
            % ind is the column representing the feature index
            % if jth index of x(i) training example >= s
            if X(i,ind) >= thresh
                predicted_x_i = 1;
            else
                predicted_x_i = -1;
            end
            if y(i) * predicted_x_i == 1
                sum_w_plus = sum_w_plus + p_dist(i);
            else
                sum_w_minus = sum_w_minus + p_dist(i);
            end
        end
        newest_theta_param = 0.5 * log(sum_w_plus / sum_w_minus);
        % Change this line so that newest_theta_param takes
        % the optimal weight for the new decision stump.

        new_theta = newest_theta_param;
        % Modify this to use the optimal weight for the newest
        % decision stump.

        % ---- No need to touch this ---- %
        theta = [theta; new_theta];
        feature_inds = [feature_inds; ind];
        thresholds = [thresholds; thresh];
        losses_per_example = exp(-y .* (...
        sign(X(:, feature_inds) - repmat(thresholds', mm, 1)) * theta));
        fprintf(1, 'Iter %d, empirical risk = %1.4f, empirical error = %1.4f\n', ...
        iter, sum(losses_per_example), sum(losses_per_example >= 1));
    end
