% Before using this method, set num_train to be the number of training
% examples you wish to read.
[sparseTrainMatrix, tokenlist, trainCategory] = ...
    readMatrix(sprintf('MATRIX.TRAIN.%d', num_train));

% Make y be a vector of +/-1 labels and X be a {0, 1} matrix.
ytrain = (2 * trainCategory - 1)';
Xtrain = 1.0 * (sparseTrainMatrix > 0);

numTrainDocs = size(Xtrain, 1);
numTokens = size(Xtrain, 2);

% Xtrain is a (numTrainDocs x numTokens) sparse matrix.
% Each row represents a unique document (email).
% The j-th column of the row $i$ represents if the j-th token appears in
% email i.

% tokenlist is a long string containing the list of all tokens (words).
% These tokens are easily known by position in the file TOKENS_LIST

% trainCategory is a (1 x numTrainDocs) vector containing the true
% classifications for the documents just read in. The i-th entry gives the
% correct class for the i-th email (which corresponds to the i-th row in
% the document word matrix).

% Spam documents are indicated as class 1, and non-spam as class 0.
% For the SVM, we convert these to +1 and -1 to form the numTrainDocs x 1
% vector ytrain.

% This vector should be output by this method
average_alpha = zeros(numTrainDocs, 1);

%---------------
% YOUR CODE HERE
tau = 8;
steps = 40 * numTrainDocs;
lambda = 1 / (64 * numTrainDocs);

Xtrain = full(Xtrain);

% GENERATING K
K = zeros(numTrainDocs, numTrainDocs);

const = - 1/(2 * tau ^ 2);
for i = 1:numTrainDocs
  Xi_repeated = repmat(Xtrain(i,:),[numTrainDocs,1]);

  a = sum(((Xtrain - Xi_repeated) .^ 2), 2);
  innerBit = const * a;

  K(:,i) = exp(innerBit);
end

% K12 = exp( const * (norm(Xtrain(5,:) - Xtrain(7,:)) ^ 2))

alpha = zeros(numTrainDocs, 1);
for step = 1:steps
  step_size = 1 / sqrt(step);
  i = randi([1,numTrainDocs]);

  Ki = K(:,i);
  % Derivative of regularisation part
  reg_d = lambda * Ki * alpha(i) ;
  % Derivative of summation part
  subgrad = zeros(numTrainDocs, 1);
  yi = ytrain(i);
  if (( yi * Ki' * alpha) < 1)
    subgrad = -yi * Ki;
  end

  alpha = alpha - step_size * (subgrad + reg_d);
  average_alpha = average_alpha + alpha;
end

average_alpha = average_alpha / (steps);

%---------------
