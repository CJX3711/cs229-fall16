wholeErrors = zeros(6,1);
labels = [ 50; 100; 200; 400; 800; 1400 ];

for iter = 1:6
  num_train = labels(iter);
  file = sprintf('MATRIX.TRAIN.%d', num_train);
  nb_train
  file = 'MATRIX.TEST';
  nb_test
  wholeErrors(iter,1) = error;
end

figure;
hold;
title('Naive Bayes Error Plot')
ylabel('Error rate');
xlabel('Training Examples');
plot(labels, wholeErrors);
