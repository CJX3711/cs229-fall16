[spmatrix, tokenlist, category] = readMatrix(file);

testMatrix = full(spmatrix);
numTestDocs = size(testMatrix, 1);
numTokens = size(testMatrix, 2);

% Assume nb_train.m has just been executed, and all the parameters computed/needed
% by your classifier are in memory through that execution. You can also assume
% that the columns in the test set are arranged in exactly the same way as for the
% training set (i.e., the j-th column represents the same token in the test data
% matrix as in the original training data matrix).

% Write code below to classify each document in the test set (ie, each row
% in the current document word matrix) as 1 for SPAM and 0 for NON-SPAM.

% Construct the (numTestDocs x 1) vector 'output' such that the i-th entry
% of this vector is the predicted class (1/0) for the i-th  email (i-th row
% in testMatrix) in the test set.
output = zeros(numTestDocs, 1);

%---------------
% YOUR CODE HERE

phi_y1 = phi_y;
phi_y0 = 1-phi_y;
for i = 1:numTestDocs
  x = testMatrix(i,:);

  % Find P(y = 1 | x)
  py0x = 0;
  for k = 1:numTokens
    xk = x(1,k);
    py0x = py0x + (xk * log( phi_ky0(k,1) ) );
  end
  py0x = py0x + log(phi_y0);

  % Find P(y = 0 | x)
  py1x = 0;
  for k = 1:numTokens
    xk = x(1,k);
    py1x = py1x + (xk * log( phi_ky1(k,1) ) );
  end
  py1x = py1x + log(phi_y1);

  if py1x > py0x
    output(i,1) = 1;
  else
    output(i,1) = 0;
  end
end

%---------------


% Compute the error on the test set
y = full(category);
y = y(:);
error = sum(y ~= output) / numTestDocs;

%Print out the classification error on the test set
fprintf(1, 'Test error: %1.4f\n', error);
