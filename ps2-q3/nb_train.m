[spmatrix, tokenlist, trainCategory] = readMatrix(file);

trainMatrix = full(spmatrix);
numTrainDocs = size(trainMatrix, 1);
numTokens = size(trainMatrix, 2);

% trainMatrix is now a (numTrainDocs x numTokens) matrix.
% Each row represents a unique document (email).
% The j-th column of the row $i$ represents the number of times the j-th
% token appeared in email $i$.

% tokenlist is a long string containing the list of all tokens (words).
% These tokens are easily known by position in the file TOKENS_LIST

% trainCategory is a (1 x numTrainDocs) vector containing the true
% classifications for the documents just read in. The i-th entry gives the
% correct class for the i-th email (which corresponds to the i-th row in
% the document word matrix).

% Spam documents are indicated as class 1, and non-spam as class 0.
% Note that for the SVM, you would want to convert these to +1 and -1.


% YOUR CODE HERE
fullTrainCategory = full(trainCategory);
spamCount = sum(fullTrainCategory);
documentLengths = sum(trainMatrix,2); % Sum on the second axis

% Calculating phi_k|y=0
phi_ky0 = zeros(numTokens, 1);
wordsInNewsDocuments = sum((1-fullTrainCategory') .* documentLengths);
for k = 1:numTokens
  tokenKCount = trainMatrix(:,k); % Sum the kth column to get the token count
  top0 = sum((1-fullTrainCategory') .* tokenKCount);
  phi_ky0(k,1) = (top0 + 1) / (wordsInNewsDocuments + numTokens);
end

% Calculating phi_k|y=1
phi_ky1 = zeros(numTokens, 1);
wordsInSpamDocuments = sum(   fullTrainCategory'  .* documentLengths);
for k = 1:numTokens
  tokenKCount = trainMatrix(:,k); % Sum the kth column to get the token count
  top1 = sum(   fullTrainCategory'  .* tokenKCount);
  phi_ky1(k,1) = (top1 + 1) / (wordsInSpamDocuments + numTokens);
end

  % Normalise
phi_ky0 = phi_ky0 / sum(phi_ky0);
phi_ky1 = phi_ky1 / sum(phi_ky1);

phi_y = spamCount / numTrainDocs;
