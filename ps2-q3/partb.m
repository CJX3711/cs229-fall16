howSpammy = zeros(numTokens, 2);

for i = 1:numTokens
  howSpammy(i,1) = log( phi_ky1(i,1) / phi_ky0(i,1) );
  howSpammy(i,2) = i;
end

[ E, index ] = sortrows(howSpammy, -1);

disp ( E(1:5, 2) );
